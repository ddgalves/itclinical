import java.util.HashMap;
import java.util.Map;

public class Resolution {
    public static String CheckUpperCase(String InputString, int n){

        //empty string for the result
        String newString = "";

        //try catch for index out of bounds exception that ocurred when n was negative
        try {
            //iterate over all the letters in the string
            for (int i = n - 1; i < InputString.length(); i += n) {

                //checking for upper case letter and saving them in the empty variable
                if (Character.isUpperCase(InputString.charAt(i))) {
                    newString = newString + InputString.charAt(i);
                }
            }
            System.out.println(newString);


        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("String index out of bounds.");
        }

        return newString;
    }

    public static String OptimalTask1(String inputString, int n){

        String newString = "";

        try {

            for (int i = n - 1; i < inputString.length(); i += n) {

                //checking for upper case letter and special characters, saving them in the empty variable
                if (Character.isUpperCase(inputString.charAt(i)) || !Character.isLetter(inputString.charAt(i))) {
                    newString = newString + inputString.charAt(i);
                }
            }
            System.out.println(newString);

        } catch (StringIndexOutOfBoundsException e) {
            System.out.println("String index out of bounds.");
        }
        return newString;
    }

    public static String OptimalTask2(String inputString, int n) {

        String newString = "";

        // Creating a HashMap containing char, sowe can keep track of all the different letters
        HashMap<Character, Integer> charCountMap = new HashMap<Character, Integer>();

        // Converting given string to char array
        char[] strArray = inputString.toCharArray();

        //checking each char of the array
        for (char character : strArray) {
            //checking if the char is uppercase
            if(Character.isUpperCase(character)) {

                newString = newString + character;

                if (charCountMap.containsKey(character)) {

                    //adding 1 to the count of each differnet char
                    charCountMap.put(character, charCountMap.get(character) + 1);

                } else {
                    //if not in the array already add it with the value of 1
                    charCountMap.put(character, 1);
                }
            }
        }

        //printing to the console the char and his occurences
        for (Map.Entry entry : charCountMap.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }

        System.out.println(newString);

        return newString;
    }
}