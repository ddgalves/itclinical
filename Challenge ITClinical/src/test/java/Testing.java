import org.junit.Assert;
import org.junit.Test;


public class Testing {

    private static String upperCase = "ITCLiNicAl";
    private static String optimalTask1 = "!tCL1Nical";
    private static String optimalTask2 = "ItCLINiCAL";
    private static String empty = "";
    private static String bigText = "What's your story? It's all in the telling. Stories are compasses and ArchitecTure;11233274366849487";
    private static String noUpperCase = "what's your story? it's all in the telling. stories are compasses and architecture;";
    private static String date = "05/05/2022";
    private static String licensePlate = "ET-09-AP";



    @Test
    public void CheckUpperCaseTests(){
        //Upper case string
        String expected = "ITCLNA";
        String actual = Resolution.CheckUpperCase(upperCase, 1);

        String expected1 = "TLN";
        String actual1 = Resolution.CheckUpperCase(upperCase, 2);

        String expected2 = "CNA";
        String actual2 = Resolution.CheckUpperCase(upperCase, 3);

        String expected3 = "";
        String actual3 = Resolution.CheckUpperCase(upperCase, 100);

        String expected4 = "";
        String actual4 = Resolution.CheckUpperCase(upperCase, -1);

        //empty case string
        String expected5 = "";
        String actual5 = Resolution.CheckUpperCase(empty, 1);

        String expected6 = "";
        String actual6 = Resolution.CheckUpperCase(empty, -1);

        //longer text string
        String expected7 = "WISAT";
        String actual7 = Resolution.CheckUpperCase(bigText, 1);

        //no upperCase letters in the string
        String expected8 = "";
        String actual8 = Resolution.CheckUpperCase(noUpperCase, 1);

        //testing if has problems with special characters and numbers
        String expected9 = "CLN";
        String actual9 = Resolution.CheckUpperCase(optimalTask1, 1);

        //testing dates
        String expected10 = "";
        String actual10 = Resolution.CheckUpperCase(date, 1);

        //testing license plates, mixture of letters and numbers
        String expected11 = "ETAP";
        String actual11= Resolution.CheckUpperCase(licensePlate, 1);


        Assert.assertEquals(expected, actual);
        Assert.assertEquals(expected1, actual1);
        Assert.assertEquals(expected2, actual2);
        Assert.assertEquals(expected3, actual3);
        Assert.assertEquals(expected4, actual4);
        Assert.assertEquals(expected5, actual5);
        Assert.assertEquals(expected6, actual6);
        Assert.assertEquals(expected7, actual7);
        Assert.assertEquals(expected8, actual8);
        Assert.assertEquals(expected9, actual9);
        Assert.assertEquals(expected10, actual10);
        Assert.assertEquals(expected11, actual11);
    }

    @Test
    public void OptimalTask1Tests(){

        String expected1 = "!CL1N";
        String actual1 = Resolution.OptimalTask1(optimalTask1, 1);

        //testing dates
        String expected2 = "05/05/2022";
        String actual2 = Resolution.OptimalTask1(date, 1);

        //testing license plates, mixture of letters and numbers
        String expected3 = "ET-09-AP";
        String actual3= Resolution.OptimalTask1(licensePlate, 1);

        String expected4 = "LN";
        String actual4 = Resolution.OptimalTask1(optimalTask1, 2);

        String expected5 = "CN";
        String actual5 = Resolution.OptimalTask1(optimalTask1, 3);

        String expected6 = "";
        String actual6 = Resolution.OptimalTask1(optimalTask1, -1);

        String expected7 = "W'  ? I'    . S    AT;11233274366849487";
        String actual7 = Resolution.OptimalTask1(bigText, 1);

        Assert.assertEquals(expected1, actual1);
        Assert.assertEquals(expected2, actual2);
        Assert.assertEquals(expected3, actual3);
        Assert.assertEquals(expected4, actual4);
        Assert.assertEquals(expected5, actual5);
        Assert.assertEquals(expected6, actual6);
        Assert.assertEquals(expected7, actual7);
    }

    @Test
    public void OptimalTask2Tests(){

        String expected1 = "ICLINCAL";
        String actual1 = Resolution.OptimalTask2(optimalTask2, 1);

        //empty case string
        String expected2 = "";
        String actual2 = Resolution.OptimalTask2(empty, 1);

        String expected3 = "";
        String actual3 = Resolution.OptimalTask2(empty, 1);

        //longer text string
        String expected4 = "WISAT";
        String actual4 = Resolution.OptimalTask2(bigText, 1);

        //no upperCase letters in the string
        String expected5 = "";
        String actual5 = Resolution.OptimalTask2(noUpperCase, 1);

        //testing if has problems with special characters and numbers
        String expected6 = "CLN";
        String actual6 = Resolution.OptimalTask2(optimalTask1, 1);

        //testing dates
        String expected7 = "";
        String actual7 = Resolution.OptimalTask2(date, 1);

        //testing license plates, mixture of letters and numbers
        String expected8 = "ETAP";
        String actual8= Resolution.OptimalTask2(licensePlate, 1);


        Assert.assertEquals(expected1, actual1);
        Assert.assertEquals(expected2, actual2);
        Assert.assertEquals(expected3, actual3);
        Assert.assertEquals(expected4, actual4);
        Assert.assertEquals(expected5, actual5);
        Assert.assertEquals(expected6, actual6);
        Assert.assertEquals(expected7, actual7);
        Assert.assertEquals(expected8, actual8);
    }
}

